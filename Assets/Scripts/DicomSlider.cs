﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DicomSlider : MonoBehaviour {

    Sprite[] sprites;
    Image SliderImage;
    Slider MainSlider;

	// Use this for initialization
	void Start () {
        MainSlider = GameObject.Find("MainSlider").GetComponent<Slider>();
        SliderImage = GameObject.Find("SliderImage").GetComponent<Image>();
        MainSlider.onValueChanged.AddListener(delegate { SliderValueChange(); });
        sprites = Resources.LoadAll<Sprite>("DicomSlider") as Sprite[];
        SliderImage.sprite = sprites[0];
        Debug.Log(sprites.Length);
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    void SliderValueChange()
    {
        SliderImage.sprite = sprites[(int)MainSlider.value];
    }
}
