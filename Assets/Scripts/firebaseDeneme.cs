﻿using System.Collections;
using System.Collections.Generic;
using SimpleFirebaseUnity;
using SimpleFirebaseUnity.MiniJSON;

using System;
using UnityEngine;

public class firebaseDeneme : MonoBehaviour {
    Firebase firebase;
    // Use this for initialization
    void Start () {
        firebase = Firebase.CreateNew("https://anatomyplatform.firebaseio.com/", "3jmmwIbUhOU93jfSZ3QK0eGJ5eGDslYW6CT8uNHR");
        firebase.OnGetSuccess += GetOKHandler;
        firebase.OnGetFailed += GetFailHandler;
        // Create a FirebaseQueue
        FirebaseQueue firebaseQueue = new FirebaseQueue(true, 3, 1f); // if _skipOnRequestError is set to false, queue will stuck on request Get.LimitToLast(-1).
        firebaseQueue.AddQueueGet(firebase.Child("Hello", true));
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    void GetOKHandler(Firebase sender, DataSnapshot snapshot)
    {
        Debug.Log("[OK] Get from key: <" + sender.FullKey + ">");
        Debug.Log("[OK] Raw Json: " + snapshot.RawJson);

        Dictionary<string, object> dict = snapshot.Value<Dictionary<string, object>>();
        List<string> keys = snapshot.Keys;

        if (keys != null)
            foreach (string key in keys)
            {
                Debug.Log(key + " = " + dict[key].ToString());
            }
    }
    void GetFailHandler(Firebase sender, FirebaseError err)
    {
        Debug.Log("[ERR] Get from key: <" + sender.FullKey + ">,  " + err.Message + " (" + (int)err.Status + ")");
    }


}
