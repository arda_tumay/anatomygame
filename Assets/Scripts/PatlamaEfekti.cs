﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class PatlamaEfekti : MonoBehaviour {
    public Button button;
    bool isPatlak;
    float patlamaCarpan = 0.1f;
    Transform []startTransform;

    // Use this for initialization
    void Start () {
        button.onClick.AddListener(onClick);
        isPatlak = false;
        /*Transform[] allChildren = GetComponentsInChildren<Transform>();
        int i = 0;
        startTransform = new Transform[allChildren.Length];
        foreach (Transform child in allChildren)
        {
            startTransform[i++] = child.transform;
        }*/
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    void onClick()
    {
        if (isPatlak)
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().name); //Reload scene, but efficiency??
        }
        else
        {
            Transform[] allChildren = GetComponentsInChildren<Transform>();
            foreach (Transform child in allChildren)
            {
                Vector3 toplama;
                if (child.gameObject.name == "Yukari")
                {
                    toplama = new Vector3(0, patlamaCarpan, 0);
                }
                else if (child.gameObject.name == "Asagi")
                {
                    toplama = new Vector3(0, -patlamaCarpan, 0);
                }
                else if (child.gameObject.name == "Sag")
                {
                    toplama = new Vector3(patlamaCarpan, 0, 0);
                }
                else if (child.gameObject.name == "Sol")
                {
                    toplama = new Vector3(-patlamaCarpan, 0, 0);
                }
                else if (child.gameObject.name == "Arka")
                {
                    toplama = new Vector3(0, 0, patlamaCarpan);

                }
                else if (child.gameObject.name == "On")
                {
                    toplama = new Vector3(0, 0, -patlamaCarpan);
                }
                else
                {
                    toplama = new Vector3(0, 0, 0);
                }
                child.gameObject.transform.position += toplama.normalized * 20;
            }
        }
        isPatlak = !isPatlak;
    }


}
