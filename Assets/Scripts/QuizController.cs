﻿using Assets.Scripts;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;
using UnityEngine.UI;


public class QuizController : MonoBehaviour
{

    static int currentQuestionIndex;
    static Question currentQuestion;
    static QuestionAnswer correctAnswer;
    static Question question;
    static Boolean loading1;
    static Boolean loading2;

    //public Text questionText;
    Button[] butonlar;
    public GameObject prefabButton;


    Quiz quiz;

    // Use this for initialization
    void Start()
    {
        loading1 = false;
        loading2 = false;
        // GameObject.Find("HomeButton").GetComponent<HomeButton>().SendMessage("HideItself");

        quiz = (Quiz)GlobalDataHolder.GetRouteParams("quiz");
        //Debug.Log(quiz.answers.Length);

        currentQuestionIndex = 0;
        currentQuestion = (quiz.questions[currentQuestionIndex]);
        renderQuestion();

        GameObject.Find("gorseller").GetComponent<Button>().onClick.AddListener(onResimlerClicked);
    }

    void OnGUI()
    {


    }


    IEnumerator getImage(string url)
    {
        //string media_url = "https://firebasestorage.googleapis.com/v0/b/anatomyplatform.appspot.com/o/images%2F4142acfa-b810-4291-96fc-5472d6ffd85a.jpg?alt=media&token=17fb8dde-6aae-47f0-b24f-d06450860b25";
        UnityWebRequest request = UnityWebRequestTexture.GetTexture(url);
        yield return request.SendWebRequest();
        if (request.isNetworkError || request.isHttpError)
        {
            StartCoroutine(getImage(currentQuestion.media.data_url));
            Debug.Log(request.error);

        }
        else
        {
            Texture2D tex = ((DownloadHandlerTexture)request.downloadHandler).texture;

            //GameObject image = new GameObject();

            //image.name = "image";
            //image.transform.SetParent(GameObject.Find("Canvas").transform);

            //image.AddComponent<RawImage>();
            Texture2D webTexture = ((DownloadHandlerTexture)request.downloadHandler).texture as Texture2D;
            Sprite webSprite = SpriteFromTexture2D(webTexture);


            GameObject.Find("questionImage").GetComponent<Image>().sprite = webSprite;
            loading1 = false;
            // image.AddComponent<Image>();
            // image.GetComponent<Image>().sprite =  Sprite.Create(tex, new Rect(0, 0, tex.width, tex.height), new Vector2(0, 0));

            //image.AddComponent<MeshRenderer>();
            //image.GetComponent<MeshRenderer>().material.mainTexture = tex;
            //Instantiate(image);


        }
    }

    IEnumerator getCorrectAnswer(Question question)
    {
        UnityWebRequest request = UnityWebRequest.Get("http://188.166.49.57:8080/CorrectAnswers/" + question.id);
        yield return request.SendWebRequest();
        if (request.isNetworkError || request.isHttpError)
        {
            Debug.Log(request.error);
            StartCoroutine(getCorrectAnswer(currentQuestion));
        }
        else
        {
            QuestionAnswer result = JsonConvert.DeserializeObject<QuestionAnswer>(request.downloadHandler.text);
            correctAnswer = result;
            Debug.Log("result.atext: " + result.answer.atext);
            loading2 = false;

        }
    }

    Sprite SpriteFromTexture2D(Texture2D texture)
    {

        return Sprite.Create(texture, new Rect(0.0f, 0.0f, texture.width, texture.height), new Vector2(0.5f, 0.5f), 100.0f);
    }


    void renderQuestion()
    {

        try
        {
            loading1 = true;
            loading2 = true;
            StartCoroutine(getImage(currentQuestion.media.data_url));
            StartCoroutine(getCorrectAnswer(currentQuestion));



            GameObject[] g = GameObject.FindGameObjectsWithTag("Option");
            for (int i = 0; i < g.Length; i++)
            {
                Destroy(g[i]);
            }
        }
        catch (Exception e)
        {
            Debug.Log(e);
        }

        butonlar = new Button[currentQuestion.answers.Count];

        GameObject.Find("questionText").GetComponent<Text>().text = "Soru " + (currentQuestionIndex + 1) + ": " + currentQuestion.qtext;

        //GameObject.Find("questionImage").GetComponent<Image>().rectTransform.sizeDelta = new Vector2(1,1);



        //GameObject.Find("questionImage").GetComponent<Image>().sprite = currentQuestion.imageOfQuestion;

        GameObject.Find("gorseller").GetComponent<CanvasGroup>().interactable = false;
        GameObject.Find("gorseller").GetComponent<CanvasGroup>().alpha = 0;
        GameObject.Find("gorseller").GetComponent<Button>().onClick.AddListener(onResimlerClicked);
        /* for(int i = 0; i < butonlar.Length; i++)
         {
             butonlar[i].onClick.AddListener(delegate() { this.onButonClick(butonlar[i]); });
             /*if(butonlar[i].GetComponent<Text>().text== "Pseudounipolar Neuron" + currentAnswer)
             {
                 currentAnswer = i + 1;
             }
         }*/

        for (int i = 0; i < currentQuestion.answers.Count; i++)//burda kaldn
        {
            GameObject gButton = (GameObject)Instantiate(prefabButton, GameObject.Find("Canvas").transform, false);
            gButton.tag = "Option";
            gButton.transform.SetParent(GameObject.Find("Canvas").transform);
            gButton.GetComponentInChildren<Text>().text = currentQuestion.answers[i].atext;
            int carpan = 100 * 4 / currentQuestion.answers.Count;
            gButton.transform.position = new Vector3(gButton.transform.position.x - 20, gButton.transform.position.y - carpan * i, gButton.transform.position.z);
            gButton.GetComponent<Button>().onClick.AddListener(delegate () { this.onButonClick(gButton); });
        }
    }
    public static Texture2D LoadPNG(string filePath)
    {

        Texture2D tex = null;
        byte[] fileData;

        if (File.Exists(filePath))
        {
            fileData = File.ReadAllBytes(filePath);
            tex = new Texture2D(2, 2);
            tex.LoadImage(fileData); //..this will auto-resize the texture dimensions.
        }
        return tex;
    }

    void onResimlerClicked()
    {

        GlobalDataHolder.PutRouteParams("backToRoute", "quiz");//value must be the name of current scene
        SceneManager.LoadScene("Resimler");
    }


    public void onButonClick(GameObject b)
    {
        Debug.Log(correctAnswer.answer.atext);

        if (loading1 || loading2)
        {
            return;
        }
        if (b.GetComponentInChildren<Text>().text == correctAnswer.answer.atext)
        {

            b.GetComponent<Button>().image.color = Color.green;
            b.GetComponent<Button>().interactable = false;
            currentQuestionIndex++;

            if (currentQuestionIndex < quiz.questions.Count)
            {
                currentQuestion = ((Question)quiz.questions[currentQuestionIndex]);
                renderQuestion();
            }
            else
            {
                GameObject.Find("questionText").GetComponent<Text>().text = "TEBRİKLER!";
                GameObject.Find("gorseller").GetComponent<CanvasGroup>().interactable = true;
                GameObject.Find("gorseller").GetComponent<CanvasGroup>().alpha = 1;
                GameObject.Find("HomeButton").GetComponent<HomeButton>().SendMessage("ShowItself");
            }
        }
        else
        {
            if (GameObject.Find("questionText").GetComponent<Text>().text == "TEBRİKLER!")
            {

            }
            else
            {
                string soru = GameObject.Find("questionText").GetComponent<Text>().text;
                b.GetComponent<Button>().image.color = Color.red;
                if (!soru.Contains("Olmadı, lütfen tekrar dene"))
                {
                    GameObject.Find("questionText").GetComponent<Text>().text = "Olmadı, lütfen tekrar dene, " + soru;
                }
            }


        }
    }
    // Update is called once per frame
    void Update()
    {
        if (!loading1 && !loading2)
        {
            GameObject.Find("loading1").GetComponent<CanvasGroup>().alpha = 0;
            GameObject.Find("loading1").GetComponent<CanvasGroup>().interactable = false;

        }
        else
        {
            GameObject.Find("loading1").GetComponent<CanvasGroup>().alpha = 1;
            GameObject.Find("loading1").GetComponent<CanvasGroup>().interactable = true;
        }
        //   currentQuestion = ((Question)quiz.questions[currentQuestionIndex]);
    }
}
