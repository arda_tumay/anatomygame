﻿using Assets.Scripts;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;


public class ThumbnailActionOyunlar : MonoBehaviour
{

    public string gameScene;
    public GlobalDataHolder.Sistemler videoCategory;

    // Use this for initialization
    void Start()
    {
        GetComponent<Button>().onClick.AddListener(PlayGame);
    }

    // Update is called once per frame
    void Update()
    {

    }

    void ShowItselfBasedOnSistem(GlobalDataHolder.Sistemler selectedSistem)
    {
        if (videoCategory == selectedSistem)
        {
            ShowItself();
        }
        else
        {
            HideItselfOnDifferentSistem();
        }
    }

    void PlayGame()
    {
        if (gameObject.name == "FirstQuiz")
        {

            /*string[] ops = { "Medulla Oblongata", "Lateral spinothalamic tract", "Cervical spinal cord", "Thalamus" };
            Quizold quiz = new Quizold(Quizold.quizTypes.Normal);
            Sprite sprites = Resources.Load<Sprite>("lateral") as Sprite;
            Debug.Log("sprite: " + sprites.ToString());

            quiz.addQuestion(new Questionold("1 numaralı yapıyı seçiniz", ops, "hint", "relatedımages", sprites, 0));
            quiz.addQuestion(new Questionold("2 numaralı yapıyı seçiniz", ops, "hint", "relatedımages", sprites, 3));
            quiz.addQuestion(new Questionold("3 numaralı yapıyı seçiniz", ops, "hint", "relatedımages", sprites, 1));
            quiz.addQuestion(new Questionold("4 numaralı yapıyı seçiniz", ops, "hint", "relatedımages", sprites, 2));*/

            GlobalDataHolder.PutRouteParams("question", "");
        }
        SceneManager.LoadScene(gameScene);
    }

    void HideItself()
    {
        CanvasGroup thumbCanvasGroup = GetComponent<CanvasGroup>();
        gameObject.GetComponent<CanvasGroup>().alpha = 0;
        gameObject.GetComponent<CanvasGroup>().interactable = false;
    }

    void ShowItself()
    {
        CanvasGroup thumbCanvasGroup = GetComponent<CanvasGroup>();
        thumbCanvasGroup.alpha = 1;
        thumbCanvasGroup.interactable = true;
    }

    void HideItselfOnDifferentSistem()
    {
        gameObject.SetActive(false);
    }

}
