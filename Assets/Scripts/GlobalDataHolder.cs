﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class GlobalDataHolder  {
        
    public enum  Sistemler { Yapilar, IskeletSistemi, DolasimSistemi, SinirSistemi, UrogenitalSistem, SolunumSistemi, EndokrinSistem, SindirimSistemi, PatolojikOgeler, HareketSistemi };
    public enum MediaType
    {
        Image, Video
    }

    private static Dictionary<string, object> routeParams = new Dictionary<string, object>();

    public static void PutRouteParams(string key, object value)
    {
        routeParams[key] = value;
    }

    public static object GetRouteParams(string key)
    {
        object value;
        bool isKeyFound = routeParams.TryGetValue(key, out value);
        return isKeyFound ? value : isKeyFound;
    }
    
    public static bool RemoveRouteParams(string key)
    {
        return routeParams.Remove("someKeyName");
    }

    public static bool isKeyExist(string key)
    {
        return routeParams.ContainsKey(key);
    }

    public static void ClearGlobalDataHolder()
    {
        routeParams.Clear();
    }
}
