﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DicomSliderController : MonoBehaviour
{
    SystemObj selectedSistemOBJ;


    // Start is called before the first frame update
    void Start()
    {
        selectedSistemOBJ = (SystemObj)GlobalDataHolder.GetRouteParams("selectedSistemOBJ");
        foreach (GameObject Obj in GameObject.FindGameObjectsWithTag("Dicom"))
        {
            if (Obj.GetComponent<Attributes>().system_id == selectedSistemOBJ.id)
            {
                Obj.transform.SetParent(transform);
                Obj.GetComponent<RectTransform>().sizeDelta = new Vector2(160, 100);
                Obj.GetComponent<RectTransform>().localScale = new Vector3(1,1,1);
            }
            else
            {
                hide(Obj);
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void hide(GameObject obj)
    {
        obj.GetComponent<CanvasGroup>().interactable = false;
        obj.GetComponent<CanvasGroup>().alpha = 0;
    }
}
