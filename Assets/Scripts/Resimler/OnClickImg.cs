﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;

public class OnClickImg : MonoBehaviour, IPointerClickHandler
{
    bool clickControl;
    Vector3 originalPos;
    GameObject scrollContent;
    public Media currentImg;
    public Texture2D texture;

    // Start is called before the first frame update
    void Start()
    {
        clickControl = false;
        originalPos = new Vector3(gameObject.transform.localPosition.x, gameObject.transform.localPosition.y, gameObject.transform.localPosition.z);
        scrollContent = GameObject.Find("Content");
        //Debug.Log(originalPos);
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void OnPointerClick(PointerEventData eventData)
    {
        RectTransform scrollContentRectT = scrollContent.GetComponent<RectTransform>();
        Debug.Log("click on image");

        GlobalDataHolder.PutRouteParams("resim", currentImg);
        GlobalDataHolder.PutRouteParams("texture", texture);
        SceneManager.LoadScene("ResimOpen");
        // OnClick code goes here ...
        if (!clickControl)
        {
            GetComponent<RectTransform>().sizeDelta = new Vector2(250, 250);
            //gameObject.transform.localPosition = new Vector3(canvas.GetComponent<RectTransform>().rect.width * 0.5f, canvas.GetComponent<RectTransform>().rect.height * 0.5f, 0);
            gameObject.transform.localPosition = new Vector3(scrollContentRectT.rect.x + scrollContentRectT.rect.width * 0.5f, scrollContentRectT.rect.y + scrollContentRectT.rect.height * 0.5f, 10);
            // Debug.Log(scrollContentRectT.rect.x + scrollContentRectT.rect.width * 0.5f + "   " + scrollContentRectT.rect.y + scrollContentRectT.rect.height * 0.5f);
            clickControl = true;
        }
        else
        {
            GetComponent<RectTransform>().sizeDelta = new Vector2(100, 100);
            gameObject.transform.localPosition = originalPos;
            clickControl = false;
        }


    }
}
