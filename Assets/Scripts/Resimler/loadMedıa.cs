﻿using Newtonsoft.Json;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class loadMedıa : MonoBehaviour
{

    SystemObj selectedSistemOBJ;
    List<Media> medias;
    public GameObject imagePrefab;
    public GameObject videoPrefab;
    bool noVideoThumbmbail;
    [HideInInspector]
    public enum media_type { video, image };
    public media_type type;
    public Camera camera;


    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
    private void Awake()
    {
        selectedSistemOBJ = (SystemObj)GlobalDataHolder.GetRouteParams("selectedSistemOBJ");
        if (type == media_type.image)
        {
            Debug.Log("RESİMDESİN");
            StartCoroutine(GetPictures(selectedSistemOBJ.id));

        }
        else if (type == media_type.video)
        {
            Debug.Log("VİDEODASIN");
            StartCoroutine(GetVideos(selectedSistemOBJ.id));
        }
    }

    IEnumerator GetPictures(int selectedSistemId)
    {
        UnityWebRequest request = UnityWebRequest.Get("http://188.166.49.57:8080/Media/GetBySystemAndMediaType?system_id=" + selectedSistemId + "&mediatype_id=" + (int)GlobalDataHolder.MediaType.Image);
        Debug.Log(request.url);
        yield return request.SendWebRequest();

        if (request.isNetworkError || request.isHttpError)
        {
            Debug.Log(request.error);
        }
        else
        {
            // Show results as text
            string mediaString = request.downloadHandler.text;
            //JArray a = JArray.Parse(systemsString);

            //  Debug.Log(a.ToString());
            List<Media> result = JsonConvert.DeserializeObject<List<Media>>(mediaString);
            Debug.Log(mediaString);
            result.ForEach((response) =>
            {
                Debug.Log(response.data_url.ToString());
            });
            medias = result;


            for (int i = 0; i < medias.Count; i++)
            {
                string media_url = medias[i].data_url;
                UnityWebRequest mediaRequest = UnityWebRequestTexture.GetTexture(media_url);
                yield return mediaRequest.SendWebRequest();
                if (mediaRequest.isNetworkError || mediaRequest.isHttpError)
                    Debug.Log(mediaRequest.error);
                else
                {

                    Texture2D tex = ((DownloadHandlerTexture)mediaRequest.downloadHandler).texture;
                    GameObject img = Instantiate(imagePrefab, transform) as GameObject;
                    img.GetComponent<RawImage>().texture = ((DownloadHandlerTexture)mediaRequest.downloadHandler).texture;
                    img.GetComponent<OnClickImg>().currentImg = medias[i];
                    img.GetComponent<OnClickImg>().texture = ((DownloadHandlerTexture)mediaRequest.downloadHandler).texture;
                }
            }
            /* foreach (KeyValuePair<string, object> d in systems)
             {
                 // if (d.Value.GetType().FullName.Contains("Newtonsoft.Json.Linq.JObject"))
                 if (d.Value is JObject)
                 {
                     Debug.Log(d.Value);
                 }
                 else
                 {
                     Debug.Log(d.Value);
                 }
             }*/

            // Or retrieve results as binary data
            /* byte[] results = www.downloadHandler.data;
             foreach(int i in results)
             {
                 Debug.Log((char)results[i]);

             }*/
        }
    }

    IEnumerator GetVideos(int selectedSistemId)
    {
        UnityWebRequest www = UnityWebRequest.Get("http://188.166.49.57:8080/Media/GetBySystemAndMediaType?system_id=" + selectedSistemId + "&mediatype_id=" + (int)GlobalDataHolder.MediaType.Video);
        yield return www.SendWebRequest();

        if (www.isNetworkError || www.isHttpError)
        {
            Debug.Log(www.error);
        }
        else
        {
            // Show results as text
            string mediaString = www.downloadHandler.text;
            //JArray a = JArray.Parse(systemsString);

            //  Debug.Log(a.ToString());
            List<Media> result = JsonConvert.DeserializeObject<List<Media>>(mediaString);
            Debug.Log(mediaString);
            result.ForEach((response) =>
            {
                // Debug.Log(response.data_url.ToString());
            });
            medias = result;
            if (medias.Count != 0)
            {
                noVideoThumbmbail = true;
                int offset = 0;
                for (int i = 0; i < medias.Count; i++)
                {
                    string media_url = medias[i].data_url;
                    offset = 120;
                    GameObject vdo = Instantiate(videoPrefab, transform) as GameObject;
                    vdo.transform.localPosition = new Vector3(vdo.transform.localPosition.x, vdo.transform.localPosition.y, vdo.transform.localPosition.z);
                    vdo.GetComponent<VideoButtonClick>().video = medias[i];
                    vdo.GetComponentInChildren<Text>().text = medias[i].description;
                    // vdo.GetComponent<ThumbnailActionVideolar>().SetVideoUrl(media_url);
                    //vdo.GetComponent<ThumbnailActionVideolar>().SetVideoName(medias[i].description);
                    //vdo.GetComponent<ThumbnailActionVideolar>().ShowItself();
                }
            }
            else
            {
                noVideoThumbmbail = false;

            }


            /* foreach (KeyValuePair<string, object> d in systems)
             {
                 // if (d.Value.GetType().FullName.Contains("Newtonsoft.Json.Linq.JObject"))
                 if (d.Value is JObject)
                 {
                     Debug.Log(d.Value);
                 }
                 else
                 {
                     Debug.Log(d.Value);
                 }
             }*/

            // Or retrieve results as binary data
            /* byte[] results = www.downloadHandler.data;
             foreach(int i in results)
             {
                 Debug.Log((char)results[i]);

             }*/
        }
    }

    public int resWidth = 150;//2550;
    public int resHeight = 150;//3300;

    void takeSS()
    {

        RenderTexture rt = new RenderTexture(resWidth, resHeight, 24);
        camera.targetTexture = rt;
        Texture2D screenShot = new Texture2D(resWidth, resHeight, TextureFormat.RGB24, false);
        camera.Render();
        RenderTexture.active = rt;
        screenShot.ReadPixels(new Rect(0, 0, resWidth, resHeight), 0, 0);
        camera.targetTexture = null;
        RenderTexture.active = null; // JC: added to avoid errors
        Destroy(rt);
        byte[] bytes = screenShot.EncodeToPNG();
        string filename = ScreenShotName(resWidth, resHeight);
        System.IO.File.WriteAllBytes(filename, bytes);
        Debug.Log(string.Format("Took screenshot to: {0}", filename));
        //takeHiResShot = false;

    }

    public static string ScreenShotName(int width, int height)
    {
        return string.Format("{0}/screenshots/screen_{1}x{2}_{3}.png",
                             Application.dataPath,
                             width, height,
                             System.DateTime.Now.ToString("yyyy-MM-dd_HH-mm-ss"));
    }

}
