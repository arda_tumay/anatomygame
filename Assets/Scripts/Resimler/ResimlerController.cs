﻿using Newtonsoft.Json;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ResimlerController : MonoBehaviour
{

    object selectedSistem;
    SystemObj selectedSistemOBJ;
    string backToRoute;
    GameObject backButton;
    GameObject homeButton;
    List<Media> medias;
    public GameObject imagePrefab;

    // Use this for initialization
    void Start()
    {

        homeButton = GameObject.Find("HomeButton");
        backButton = GameObject.Find("BackButton");
        backButton.GetComponent<Button>().onClick.AddListener(onClickBackButton);
        if (GlobalDataHolder.isKeyExist("backToRoute"))//check wheter resimler is loaded from a scene different than main scene. If so, get back route name an enable back button
        {
            string backRoute = (string)GlobalDataHolder.GetRouteParams("backToRoute");
            homeButton.GetComponent<HomeButton>().SendMessage("HideItself");
            backButton.GetComponent<CanvasGroup>().interactable = true;
            backButton.GetComponent<CanvasGroup>().alpha = 1;
            backToRoute = backRoute;

        }
        else
        {
            backButton.GetComponent<CanvasGroup>().interactable = false;
            backButton.GetComponent<CanvasGroup>().alpha = 0;
        }
    }


    void Awake()
    {
        medias = new List<Media>();
        selectedSistem = GlobalDataHolder.GetRouteParams("selectedSistem");
        selectedSistemOBJ = (SystemObj)GlobalDataHolder.GetRouteParams("selectedSistemOBJ");
        GameObject SceneHeader = GameObject.Find("SceneHeader");
        SceneHeader.GetComponent<Text>().text = selectedSistemOBJ.name;
        //StartCoroutine(GetPictures(selectedSistemOBJ.id));
        //StartCoroutine(loadImage());

    }

    // Update is called once per frame
    void Update()
    {

    }

    void onClickBackButton()
    {
        SceneManager.LoadScene(backToRoute);
    }

    IEnumerator loadImage()
    {


        string media_url = "https://firebasestorage.googleapis.com/v0/b/anatomyplatform.appspot.com/o/images%2F4142acfa-b810-4291-96fc-5472d6ffd85a.jpg?alt=media&token=17fb8dde-6aae-47f0-b24f-d06450860b25";
        UnityWebRequest request = UnityWebRequestTexture.GetTexture(media_url);
        yield return request.SendWebRequest();
        if (request.isNetworkError || request.isHttpError)
            Debug.Log(request.error);
        else
        {
            Texture2D tex = ((DownloadHandlerTexture)request.downloadHandler).texture;

            //GameObject image = new GameObject();
            GameObject img = Instantiate(imagePrefab, (GameObject.Find("Canvas").transform)) as GameObject;
            //image.name = "image";
            //image.transform.SetParent(GameObject.Find("Canvas").transform);

            //image.AddComponent<RawImage>();
            img.GetComponent<RawImage>().texture = ((DownloadHandlerTexture)request.downloadHandler).texture;

            // image.AddComponent<Image>();
            // image.GetComponent<Image>().sprite =  Sprite.Create(tex, new Rect(0, 0, tex.width, tex.height), new Vector2(0, 0));

            //image.AddComponent<MeshRenderer>();
            //image.GetComponent<MeshRenderer>().material.mainTexture = tex;
            //Instantiate(image);


        }


    }

    IEnumerator GetPictures(int selectedSistemId)
    {
        UnityWebRequest request = UnityWebRequest.Get("http://188.166.49.57:8080/Media/GetBySystemAndMediaType?system_id=" + selectedSistemId + "&mediatype_id=" + (int)GlobalDataHolder.MediaType.Image);
        yield return request.SendWebRequest();

        if (request.isNetworkError || request.isHttpError)
        {
            Debug.Log(request.error);
        }
        else
        {
            // Show results as text
            string mediaString = request.downloadHandler.text;
            //JArray a = JArray.Parse(systemsString);

            //  Debug.Log(a.ToString());
            List<Media> result = JsonConvert.DeserializeObject<List<Media>>(mediaString);
            Debug.Log(mediaString);
            result.ForEach((response) =>
            {
                Debug.Log(response.data_url.ToString());
            });
            medias = result;

            int columnNo = 0;
            int columnOffset = 120 * columnNo;
            int numOfElementsPerRow = 0;
            int numOfElementPerColumn = 0;
            int rowNo = 0;
            int rowOfset = 120 * rowNo;
            int contentHeight = 300;
            for (int i = 0; i < medias.Count; i++)
            {
                string media_url = medias[i].data_url;
                UnityWebRequest mediaRequest = UnityWebRequestTexture.GetTexture(media_url);
                yield return mediaRequest.SendWebRequest();
                if (mediaRequest.isNetworkError || mediaRequest.isHttpError)
                    Debug.Log(mediaRequest.error);
                else
                {

                    Texture2D tex = ((DownloadHandlerTexture)mediaRequest.downloadHandler).texture;
                    GameObject img = Instantiate(imagePrefab, (GameObject.Find("Content").transform)) as GameObject;
                    img.transform.localPosition = new Vector3(img.transform.localPosition.x + columnOffset, img.transform.localPosition.y - 15 - rowOfset, img.transform.localPosition.z);
                    img.GetComponent<RawImage>().texture = ((DownloadHandlerTexture)mediaRequest.downloadHandler).texture;
                    columnNo++;
                    columnOffset = 120 * columnNo;
                    numOfElementsPerRow++;
                    if (numOfElementsPerRow == 3)
                    {
                        columnNo = 0;
                        columnOffset = 120 * columnNo;
                        numOfElementsPerRow = 0;
                        rowNo++;
                        rowOfset = 120 * rowNo;
                        numOfElementPerColumn++;
                    }
                    if (numOfElementPerColumn > 2)
                    {
                        //Debug.Log(GameObject.Find("Content").GetComponent<Rect>().height);
                        GameObject.Find("Content").GetComponent<RectTransform>().sizeDelta = new Vector2(-3, contentHeight + 120);
                        contentHeight = contentHeight + 120;
                    }
                }
            }
            /* foreach (KeyValuePair<string, object> d in systems)
             {
                 // if (d.Value.GetType().FullName.Contains("Newtonsoft.Json.Linq.JObject"))
                 if (d.Value is JObject)
                 {
                     Debug.Log(d.Value);
                 }
                 else
                 {
                     Debug.Log(d.Value);
                 }
             }*/

            // Or retrieve results as binary data
            /* byte[] results = www.downloadHandler.data;
             foreach(int i in results)
             {
                 Debug.Log((char)results[i]);

             }*/
        }
    }

}
