﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SystemObj {

    public int id { get; set; }
    public string name { get; set; }

    public SystemObj(int id,string name)
    {
        this.id = id;
        this.name = name;
    }
    
}
