﻿using Newtonsoft.Json;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class QuizlerController : MonoBehaviour
{

    object selectedSistem;
    SystemObj selectedSistemOBJ;
    string backToRoute;
    GameObject backButton;
    GameObject homeButton;
    List<Quiz> quizzes;
    public GameObject imagePrefab;

    // Use this for initialization
    void Start()
    {

        homeButton = GameObject.Find("HomeButton");
        backButton = GameObject.Find("BackButton");
        backButton.GetComponent<Button>().onClick.AddListener(onClickBackButton);
        if (GlobalDataHolder.isKeyExist("backToRoute"))//check wheter resimler is loaded from a scene different than main scene. If so, get back route name an enable back button
        {
            string backRoute = (string)GlobalDataHolder.GetRouteParams("backToRoute");
            homeButton.GetComponent<HomeButton>().SendMessage("HideItself");
            backButton.GetComponent<CanvasGroup>().interactable = true;
            backButton.GetComponent<CanvasGroup>().alpha = 1;
            backToRoute = backRoute;

        }
        else
        {
            backButton.GetComponent<CanvasGroup>().interactable = false;
            backButton.GetComponent<CanvasGroup>().alpha = 0;
        }
    }


    void Awake()
    {
        quizzes = new List<Quiz>();
        selectedSistem = GlobalDataHolder.GetRouteParams("selectedSistem");
        selectedSistemOBJ = (SystemObj)GlobalDataHolder.GetRouteParams("selectedSistemOBJ");
        Debug.Log(selectedSistemOBJ.name);
        GameObject SceneHeader = GameObject.Find("SceneHeader");
        SceneHeader.GetComponent<Text>().text = selectedSistemOBJ.name.ToString() + " Quizler";

        //StartCoroutine(GetPictures(selectedSistemOBJ.id));
        //StartCoroutine(loadImage());

    }

    // Update is called once per frame
    void Update()
    {

    }

    void onClickBackButton()
    {
        SceneManager.LoadScene(backToRoute);
    }

    IEnumerator getQuizzes()
    {


        string media_url = "https://firebasestorage.googleapis.com/v0/b/anatomyplatform.appspot.com/o/images%2F4142acfa-b810-4291-96fc-5472d6ffd85a.jpg?alt=media&token=17fb8dde-6aae-47f0-b24f-d06450860b25";
        UnityWebRequest request = UnityWebRequestTexture.GetTexture(media_url);
        yield return request.SendWebRequest();
        if (request.isNetworkError || request.isHttpError)
            Debug.Log(request.error);
        else
        {
            Texture2D tex = ((DownloadHandlerTexture)request.downloadHandler).texture;

            //GameObject image = new GameObject();
            GameObject img = Instantiate(imagePrefab, (GameObject.Find("Canvas").transform)) as GameObject;
            //image.name = "image";
            //image.transform.SetParent(GameObject.Find("Canvas").transform);

            //image.AddComponent<RawImage>();
            img.GetComponent<RawImage>().texture = ((DownloadHandlerTexture)request.downloadHandler).texture;

            // image.AddComponent<Image>();
            // image.GetComponent<Image>().sprite =  Sprite.Create(tex, new Rect(0, 0, tex.width, tex.height), new Vector2(0, 0));

            //image.AddComponent<MeshRenderer>();
            //image.GetComponent<MeshRenderer>().material.mainTexture = tex;
            //Instantiate(image);


        }


    }



}
