﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class clickSkull : MonoBehaviour {

    private Text info;
    private static string eskiObject="";
    //Attributes att = new Attributes();
    // Use this for initialization
    void Start () {
        info = GameObject.FindWithTag("SkullInfo").GetComponent<Text>();
        
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnMouseDown()
    {
        // this object was clicked - do something

       // Debug.Log(gameObject.name);
        string name = gameObject.name;
        string realName = gameObject.GetComponent<Attributes>().ObjName;
        if (eskiObject != "" && eskiObject!=gameObject.name)
        {
            GameObject.Find(eskiObject).GetComponent<Renderer>().material.SetColor("_Color", Color.white);

        }
        eskiObject = gameObject.name;
        gameObject.GetComponent<Renderer>().material.SetColor("_Color", Color.red);
        if (realName == "")
        {
            realName = name;
        }
        info.text = realName;
        //Debug.Log(name);
    }


}
