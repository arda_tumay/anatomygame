﻿using Newtonsoft.Json;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LoadQuizzes : MonoBehaviour
{

    SystemObj selectedSistemOBJ;
    List<Quiz> quizzes;
    public GameObject imagePrefab;


    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
    private void Awake()
    {
        selectedSistemOBJ = (SystemObj)GlobalDataHolder.GetRouteParams("selectedSistemOBJ");
        StartCoroutine(GetQuizzes(selectedSistemOBJ.id));
    }

    void onQuizButtonClick(GameObject index)
    {
        Debug.Log("index: " + index);
        GlobalDataHolder.PutRouteParams("quiz", quizzes[int.Parse(index.GetComponent<Attributes>().ObjName)]);
        SceneManager.LoadScene("quiz");
    }


    IEnumerator GetQuizzes(int selectedSistemId)
    {
        UnityWebRequest request = UnityWebRequest.Get("http://188.166.49.57:8080/Quizzes/SystemId/" + selectedSistemId);
        yield return request.SendWebRequest();

        if (request.isNetworkError || request.isHttpError)
        {
            Debug.Log(request.error);
        }
        else
        {
            // Show results as text
            string responseStr = request.downloadHandler.text;

            List<Quiz> result = JsonConvert.DeserializeObject<List<Quiz>>(responseStr);
            List<Quiz> test = new List<Quiz>();
            Debug.Log(responseStr);
            result.ForEach((response) =>
            {
                Debug.Log(response.header.ToString());
                test.Add(result.ToArray()[0]);
            });
            quizzes = test;

            if (quizzes.Count == 0)
            {
                GameObject.Find("NoQuizText").SetActive(true);
            }
            else
            {
                GameObject.Find("NoQuizText").SetActive(false);
            }

            for (int i = 0; i < quizzes.Count; i++)
            {
                string media_url = quizzes[i].questions[0].media.data_url;
                UnityWebRequest mediaRequest = UnityWebRequestTexture.GetTexture(media_url);
                yield return mediaRequest.SendWebRequest();
                if (mediaRequest.isNetworkError || mediaRequest.isHttpError)
                    Debug.Log(mediaRequest.error);
                else
                {

                    Texture2D tex = ((DownloadHandlerTexture)mediaRequest.downloadHandler).texture;
                    GameObject img = Instantiate(imagePrefab, transform) as GameObject;
                    img.GetComponentInChildren<RawImage>().texture = ((DownloadHandlerTexture)mediaRequest.downloadHandler).texture;
                    img.GetComponentInChildren<Attributes>().ObjName = i.ToString();
                    img.GetComponentInChildren<Button>().onClick.AddListener(delegate () { this.onQuizButtonClick(img); });
                }
            }

        }
    }
}
