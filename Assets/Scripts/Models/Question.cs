﻿using System;
using System.Collections.Generic;

public class Question
{
    public int id { get; set; }
    public string qtext { get; set; }
    public string hint { get; set; }
    public int quiz_id { get; set; }
    public List<Answer> answers { get; set; }
    public Topic topic { get; set; }
    public Media media { get; set; }




    public Question()
    {
    }

}
