﻿using Assets.Scripts;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ResimRenderController : MonoBehaviour
{
    Media image;
    Texture2D texture;
    // Start is called before the first frame update
    void Start()
    {
        image = (Media)GlobalDataHolder.GetRouteParams("resim");
        texture = (Texture2D)GlobalDataHolder.GetRouteParams("texture");
        Sprite webSprite = SpriteFromTexture2D(texture);
        GameObject.Find("questionImage").GetComponent<Image>().sprite = webSprite;
        GameObject.Find("Description").GetComponent<Text>().text = image.description;
        GameObject.Find("BackButton").GetComponent<Button>().onClick.AddListener(onClickBackButton);


    }

    // Update is called once per frame
    void Update()
    {

    }

    Sprite SpriteFromTexture2D(Texture2D texture)
    {

        return Sprite.Create(texture, new Rect(0.0f, 0.0f, texture.width, texture.height), new Vector2(0.5f, 0.5f), 100.0f);
    }

    void onClickBackButton()
    {
        SceneManager.LoadScene("Resimler");
    }


}
