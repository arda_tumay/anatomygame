﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ThumbnailActionVideolar : MonoBehaviour
{

    string videoUrl;
    public GlobalDataHolder.Sistemler videoCategory;
    // Use this for initialization
    void Start()
    {
        GetComponent<Button>().onClick.AddListener(RunVideo);
    }

    // Update is called once per frame
    void Update()
    {
    }

    public void ShowItselfBasedOnSistem(GlobalDataHolder.Sistemler selectedSistem)
    {
        if(videoCategory == selectedSistem)
        {
            ShowItself();
        }else
        {
            HideItselfOnDifferentSistem();
        }
    }

    public void RunVideo()
    {
        GameObject.Find("Canvas").SendMessage("SetVideoUrlInParent", videoUrl);
        GameObject.Find("Canvas").SendMessage("PlayVideo");
        GameObject.Find("Canvas").SendMessage("PlayingVideo", true);
    }

    public void HideItself()
    {
        CanvasGroup thumbCanvasGroup = GetComponent<CanvasGroup>();
        gameObject.GetComponent<CanvasGroup>().alpha = 0;
        gameObject.GetComponent<CanvasGroup>().interactable = false;
    }

    public void ShowItself()
    {
        CanvasGroup thumbCanvasGroup = GetComponent<CanvasGroup>();
        thumbCanvasGroup.alpha = 1;
        thumbCanvasGroup.interactable = true;
    }

    public void HideItselfOnDifferentSistem()
    {
        gameObject.SetActive(false);
    }
    public void SetVideoUrl(string url)
    {
        videoUrl = url;
    }
    public void SetVideoName(string name)
    {
        gameObject.GetComponentInChildren<Text>().text = name;
    }
}