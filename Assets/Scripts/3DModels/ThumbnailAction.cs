﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ThumbnailAction : MonoBehaviour {

    public Button Thumb;
    public string goToPage;
	// Use this for initialization
	void Start () {
        Thumb.onClick.AddListener(GoToPage);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void GoToPage()
    {
        SceneManager.LoadScene(goToPage);
    }
}
