﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class zoomInOut : MonoBehaviour {

    Camera mycamera;
    // Use this for initialization
    void Start () {
        mycamera = gameObject.GetComponent<Camera>();
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetAxis("Mouse ScrollWheel") > 0)
        {
            if (mycamera.fieldOfView > 1)
            {
                mycamera.fieldOfView--;
            }
        }
        if (Input.GetAxis("Mouse ScrollWheel") < 0)
        {
            if (mycamera.fieldOfView < 100)
            {
                mycamera.fieldOfView++;
            }
        }

    }
}
