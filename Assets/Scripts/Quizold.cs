﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using System.Collections;

namespace Assets.Scripts
{
    class Quizold : ScriptableObject
    {
        public enum quizTypes
        {
            Sıralama = 1,
            Normal = 0
        }

        public ArrayList questions;
        public String thumbnail;
        public quizTypes type;

        public Quizold(quizTypes q)
        {
            questions = new ArrayList();
            type = q;
        }

        public Quizold(ArrayList questionArr, String thumbnailArg, quizTypes q)
        {
            this.questions = questionArr;
            this.thumbnail = thumbnailArg;
            type = q;
        }

        public void addQuestion(Question q)
        {
            questions.Add(q);
        }

    }
}
