﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Attributes : MonoBehaviour
{
    public string ObjName;
    public int system_id;

    public void SetSystemId(int sysid)
    {
        system_id = sysid;
    }

    public int GetSystemId()
    {
        return this.system_id;
    }

    public void SetObjName(string objname)
    {
        ObjName = objname;
    }

    public string GetObjName()
    {
        return this.ObjName;
    }
}
