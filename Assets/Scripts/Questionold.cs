﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts
{
    class Questionold : ScriptableObject
    {

        public String question { get; set; }
        public String[] options;
        public String hint, relatedImages;
        public Sprite imageOfQuestion;
        public int answer;

        public Questionold()
        {
        }

        public Questionold(string question, string[] options, string hint, string relatedImages, Sprite imageOfQuestion, int a)
        {
            this.question = question;
            this.options = options;
            this.hint = hint;
            this.relatedImages = relatedImages;
            this.imageOfQuestion = imageOfQuestion;
            this.answer = a;
        }


    }
}
