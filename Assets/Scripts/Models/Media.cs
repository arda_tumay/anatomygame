﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Media 
{

    public int id { get; set; }
    public string data_url { get; set;}
    public string thumbnail_url { get; set; }
    public string description { get; set; }
    public List<Topic> topics { get; set; }
    public SystemObj system { get; set; }
    public int system_id { get; set; }
    public GlobalDataHolder.MediaType media_type { get; set; }
    public string date { get; set; }
}

public class Topic
{
    public int id { get; set; }
    public string name { get; set; }
}


