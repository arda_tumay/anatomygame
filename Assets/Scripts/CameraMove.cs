﻿using UnityEngine;
using System.Collections;

public class CameraMove : MonoBehaviour
{

    /*
    Writen by Windexglow 11-13-10.  Use it, edit it, steal it I don't care.  
    Converted to C# 27-02-13 - no credit wanted.
    Simple flycam I made, since I couldn't find any others made public.  
    Made simple to use (drag and drop, done) for regular keyboard layout  
    wasd : basic movement
    shift : Makes camera accelerate
    space : Moves camera on X and Z axis only.  So camera doesn't gain any height*/

    /*
    float mainSpeed = 100.0f; //regular speed
    float shiftAdd = 250.0f; //multiplied by how long shift is held.  Basically running
    float maxShift = 1000.0f; //Maximum speed when holdin gshift
    float camSens = 0.15f; //How sensitive it with mouse
    private Vector3 lastMouse = new Vector3(255, 255, 255); //kind of in the middle of the screen, rather than at the top (play)
    private float totalRun = 1.0f;
    bool isRotating = false;

    void Update()
    {
        if (Input.GetButtonDown("Fire1"))
        {
            lastMouse= Input.mousePosition;
            isRotating = true;
        }
        //Mouse  camera angle done.  
        if (!Input.GetMouseButton(0)) isRotating = false;
        //Keyboard commands
        float f = 0.0f;
        Vector3 p = GetBaseInput();
        if (Input.GetKey(KeyCode.LeftShift))
        {
            totalRun += Time.deltaTime;
            p = p * totalRun * shiftAdd;
            p.x = Mathf.Clamp(p.x, -maxShift, maxShift);
            p.y = Mathf.Clamp(p.y, -maxShift, maxShift);
            p.z = Mathf.Clamp(p.z, -maxShift, maxShift);
        }
        else
        {
            totalRun = Mathf.Clamp(totalRun * 0.5f, 1f, 1000f);
            p = p * mainSpeed;
        }

        p = p * Time.deltaTime;
        Vector3 newPosition = transform.position;
        if (Input.GetKey(KeyCode.Space))
        { //If player wants to move on X and Z axis only
            transform.Translate(p);
            newPosition.x = transform.position.x;
            newPosition.z = transform.position.z;
            transform.position = newPosition;
        }
        else
        {
            transform.Translate(p);
        }
        if (isRotating)
        {
            lastMouse = Input.mousePosition - lastMouse;
            lastMouse = new Vector3(-lastMouse.y * camSens, lastMouse.x * camSens, 0);
            lastMouse = new Vector3(transform.eulerAngles.x + lastMouse.x, transform.eulerAngles.y + lastMouse.y, 0);
            transform.eulerAngles = lastMouse;
            lastMouse = Input.mousePosition;
        }

    }

    private Vector3 GetBaseInput()
    { //returns the basic values, if it's 0 than it's not active.
        Vector3 p_Velocity = new Vector3();
        if (Input.GetKey(KeyCode.W))
        {
            p_Velocity += new Vector3(0, 0, 1);
        }
        if (Input.GetKey(KeyCode.S))
        {
            p_Velocity += new Vector3(0, 0, -1);
        }
        if (Input.GetKey(KeyCode.A))
        {
            p_Velocity += new Vector3(-1, 0, 0);
        }
        if (Input.GetKey(KeyCode.D))
        {
            p_Velocity += new Vector3(1, 0, 0);
        }
        return p_Velocity;
    }
    */

    #region ROTATE
    private float _sensitivity = 0.4f;
    private Vector3 _mouseReference;
    private Vector3 _mouseOffset;
    private Vector3 _rotation = Vector3.zero;
    private bool _isRotating, buttonPressed=false;
    private Collider collider;
    float speed = 100.0f;
    #endregion


    private void Start()
    {
        collider = gameObject.GetComponent<BoxCollider>();
        if (collider == null)
        {
            collider = gameObject.AddComponent<BoxCollider>();
        }

            
    }
    private float sliderLastX = 0f;
    void Update()
    {
        if (buttonPressed)
        {
           // transform.Rotate(new Vector3(0, Input.GetAxis("Mouse X"), 0) * Time.deltaTime * speed);
            Quaternion localRotation = Quaternion.Euler(-Input.GetAxis("Mouse Y"), -Input.GetAxis("Mouse X") , 0f);
            transform.rotation = transform.rotation * localRotation;
            sliderLastX = transform.rotation.x;

        }
        if (Input.GetButtonDown("Fire1"))
        {
            buttonPressed = true;
        }
        if (Input.GetButtonUp("Fire1"))
        {
            buttonPressed = false;
        }

       /* if (_isRotating)
        {

            
            // offset
            _mouseOffset = (Input.mousePosition - _mouseReference);

            // apply rotation
            _rotation.z = -(_mouseOffset.x + _mouseOffset.y) * _sensitivity;

            // rotate
            gameObject.transform.Rotate(_rotation);

            //gameObject.transform.RotateAround(transform.position, _rotation, Time.deltaTime * 90f);
            //gameObject.transform.RotateAroundLocal();

            // store new mouse position
            _mouseReference = Input.mousePosition;

           /* if(Input.GetMouseButton(0) && collider.bounds.size.magnitude > 0)
            {
                var dtx = Input.GetAxis("Mouse X") * 1;
                var dty = Input.GetAxis("Mouse Y") * 1;
                transform.RotateAround(collider.bounds.center, Vector3.up, dtx);
                transform.RotateAround(collider.bounds.center, Vector3.right, dty);

            }

        }*/
    }

  /* void OnMouseDown()
    {
        // rotating flag
        _isRotating = true;

        // store mouse position
        _mouseReference = Input.mousePosition;
    }

    void OnMouseUp()
    {
        // rotating flag
        _isRotating = false;
    }*/
}