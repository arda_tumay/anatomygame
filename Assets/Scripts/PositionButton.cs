﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;



public class PositionButton : MonoBehaviour, UnityEngine.EventSystems.IPointerDownHandler, UnityEngine.EventSystems.IPointerUpHandler
{
    Button b;
    bool buttonMovement;
    Vector3 firstPoint, lastPoint;
    
	// Use this for initialization
	void Start () {
        b = (Button) gameObject.GetComponent<Button>();
        buttonMovement = false;

    }
	
	// Update is called once per frame
	void Update () {
        if (buttonMovement)
        {
            Vector3 pos = gameObject.transform.position;
            pos.x = Input.mousePosition.x;
            pos.y = Input.mousePosition.y;

            gameObject.transform.position = pos;
        }
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        buttonMovement = true;
        firstPoint = new Vector3(Input.mousePosition.x, Input.mousePosition.y, 0);
        
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        buttonMovement = false;
        lastPoint = new Vector3(Input.mousePosition.x, Input.mousePosition.y, 0);

        Vector3 differenceVector = lastPoint - firstPoint;


    }
}
