﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class VideoButtonsController : MonoBehaviour {

    Button PlayButton;
    Button StopButton;
    Button RestartButton;

	// Use this for initialization
	void Start () {
        PlayButton = GameObject.Find("AudioPlay").GetComponent<Button>();
        StopButton = GameObject.Find("AudioStop").GetComponent<Button>();
        RestartButton = GameObject.Find("AudioRestart").GetComponent<Button>();
        PlayButton.onClick.AddListener(PlayPressed);
        StopButton.onClick.AddListener(StopPressed);
        RestartButton.onClick.AddListener(RestartPressed);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void PlayPressed(){
        GameObject.Find("Canvas").SendMessage("ControlVideo", "play");
    }

    void StopPressed()
    {
        GameObject.Find("Canvas").SendMessage("ControlVideo", "stop");

    }

    void RestartPressed()
    {
        GameObject.Find("Canvas").SendMessage("ControlVideo", "restart");

    }

    void ShowVideoButtons()
    {
        GetComponent<CanvasGroup>().alpha = 1;
        GetComponent<CanvasGroup>().interactable = true;
    }

    void HideVideoButtons()
    {
        GetComponent<CanvasGroup>().alpha = 0;
        GetComponent<CanvasGroup>().interactable = false;
    }
}
