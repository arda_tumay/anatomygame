﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class VideoButtonClick : MonoBehaviour
{
    public Media video;
    // Start is called before the first frame update
    void Start()
    {
        GetComponent<Button>().onClick.AddListener(PlayGame);
    }

    // Update is called once per frame
    void Update()
    {

    }

    void PlayGame()
    {
        GlobalDataHolder.PutRouteParams("video", video);
        SceneManager.LoadScene("VideoPlay");
    }
}
