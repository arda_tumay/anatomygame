﻿using Newtonsoft.Json;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class loadStaticResources : MonoBehaviour
{
    List<StaticResource> staticResources;
    SystemObj selectedSistemOBJ;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void Awake()
    {
        selectedSistemOBJ = (SystemObj)GlobalDataHolder.GetRouteParams("selectedSistemOBJ");
        StartCoroutine(GetStaticResources(selectedSistemOBJ.id));

    }


    IEnumerator GetStaticResources(int selectedSistemId)
    {
        UnityWebRequest www = UnityWebRequest.Get("http://188.166.49.57:8080/StaticResources/GetBySystemAndType?system_id=" + selectedSistemId + "&type_id=95");
        yield return www.SendWebRequest();

        if (www.isNetworkError || www.isHttpError)
        {
            Debug.Log(www.error);
        }
        else
        {
            // Show results as text
            string resourceString = www.downloadHandler.text;
            //JArray a = JArray.Parse(systemsString);

            //  Debug.Log(a.ToString());
            List<StaticResource> result = JsonConvert.DeserializeObject<List<StaticResource>>(resourceString);
            //Debug.Log(resourceString);
            result.ForEach((response) =>
            {
                // Debug.Log(response.data_url.ToString());
            });
            staticResources = result;
            GameObject[] models = GameObject.FindGameObjectsWithTag("3DModel");

            if (staticResources.Count != 0)
            {
                int offset = 0;
                for (int i = 0; i < staticResources.Count; i++)
                {
                    for(int j = 0; j < models.Length; j++)
                    {
                        if(models[i].GetComponent<Attributes>().system_id == staticResources[i].system_id)
                        {
                            models[i].GetComponent<CanvasGroup>().alpha = 1;
                            models[i].GetComponent<CanvasGroup>().interactable = true;
                            models[i].GetComponent<RectTransform>().localScale = new Vector3(1, 1, 1);


                        }
                    }
                    Debug.Log(staticResources[i].system_id);
                }
            }
            


            /* foreach (KeyValuePair<string, object> d in systems)
             {
                 // if (d.Value.GetType().FullName.Contains("Newtonsoft.Json.Linq.JObject"))
                 if (d.Value is JObject)
                 {
                     Debug.Log(d.Value);
                 }
                 else
                 {
                     Debug.Log(d.Value);
                 }
             }*/

            // Or retrieve results as binary data
            /* byte[] results = www.downloadHandler.data;
             foreach(int i in results)
             {
                 Debug.Log((char)results[i]);

             }*/
        }
    }



}
