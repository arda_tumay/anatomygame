﻿using System.Collections;
using System.Collections.Generic;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;

public class OyunlarController : MonoBehaviour
{

    object selectedSistem;
    bool noGameThumbmbail;

    // Use this for initialization
    void Start()
    {

    }

    void Awake()
    {
        selectedSistem = GlobalDataHolder.GetRouteParams("selectedSistem");
        Debug.Log(selectedSistem.ToString());
        GameObject SceneHeader = GameObject.Find("SceneHeader");
        SceneHeader.GetComponent<Text>().text = ((GlobalDataHolder.Sistemler)selectedSistem).ToString();

        StartCoroutine(getQuizzes());

        GameObject.FindWithTag("GameThumbnail").SendMessage("ShowItselfBasedOnSistem", selectedSistem);
        noGameThumbmbail = GameObject.FindWithTag("GameThumbnail");
    }

    // Update is called once per frame
    void Update()
    {
        if (noGameThumbmbail)
        {

        }
        else
        {

        }
    }

    IEnumerator getQuizzes()
    {
        UnityWebRequest www = UnityWebRequest.Get("http://188.166.49.57:8080/Quizzes");
        yield return www.SendWebRequest();

        if (www.isNetworkError || www.isHttpError)
        {
            Debug.Log(www.error.ToString());
        }
        else
        {
            // Show results as text
            string quizzesString = www.downloadHandler.text;
            //JArray a = JArray.Parse(systemsString);

            Debug.Log(quizzesString.ToString());

            //  Debug.Log(a.ToString());
            List<Quiz> result = JsonConvert.DeserializeObject<List<Quiz>>(quizzesString);
            List<Quiz> quizzesOfTheSystem = new List<Quiz>();
            SystemObj selected = (SystemObj)GlobalDataHolder.GetRouteParams("selectedSistemOBJ");
            foreach (Quiz q in result)
            {
                if (q.system.name == selected.name)
                {
                    quizzesOfTheSystem.Add(q);
                }
            }

            GlobalDataHolder.PutRouteParams("quizzesOfTheSystem", quizzesOfTheSystem);
            GlobalDataHolder.PutRouteParams("quiz", quizzesOfTheSystem[0]);

            //listOfSystems.Add(new Dropdown.OptionData("Yapılar"));

            /* foreach (KeyValuePair<string, object> d in systems)
             {
                 // if (d.Value.GetType().FullName.Contains("Newtonsoft.Json.Linq.JObject"))
                 if (d.Value is JObject)
                 {
                     Debug.Log(d.Value);
                 }
                 else
                 {
                     Debug.Log(d.Value);
                 }
             }*/

            // Or retrieve results as binary data
            /* byte[] results = www.downloadHandler.data;
             foreach(int i in results)
             {
                 Debug.Log((char)results[i]);

             }*/
        }
    }


}
