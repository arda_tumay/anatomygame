﻿using Newtonsoft.Json;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
using UnityEngine.Video;



public class VideolarController : MonoBehaviour
{

    GlobalDataHolder.Sistemler selectedSistem;
    SystemObj selectedSistemOBJ;
    string clickedThumbnailVideoUrl;
    Renderer videoRenderer;
    VideoPlayer video;
    public VideoClip[] videoClips;
    bool isVideoPlaying;
    GameObject SceneHeader;
    GameObject AudioButtons;
    bool noVideoThumbmbail;
    List<Media> medias;
    public GameObject videoPrefab;


    // Use this for initialization
    void Start()
    {
        Debug.Log("saassaas");
        Debug.Log(((Media)GlobalDataHolder.GetRouteParams("video")).data_url);
        PlayVideo();
    }

    void Awake()
    {
        medias = new List<Media>();
        selectedSistemOBJ = (SystemObj)GlobalDataHolder.GetRouteParams("selectedSistemOBJ");

        videoRenderer = GameObject.Find("VideoPlane").GetComponent<Renderer>();
        //videoRenderer.enabled = false;
        video = GameObject.Find("VideoPlane").GetComponent<VideoPlayer>();

        selectedSistem = (GlobalDataHolder.Sistemler)GlobalDataHolder.GetRouteParams("selectedSistem");
        SceneHeader = GameObject.Find("SceneHeader");
        string header = selectedSistem.ToString();
        SceneHeader.GetComponent<Text>().text = header;

        isVideoPlaying = false;

        AudioButtons = GameObject.Find("AudioButtons");
        //AudioButtons.SendMessage("HideVideoButtons");

        //GameObject.FindWithTag("VideoThumbnail").SendMessage("ShowItselfBasedOnSistem", selectedSistem);
        noVideoThumbmbail = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (noVideoThumbmbail)//if there is at least one video thumbnail in the scene to show to user
        {
            if (isVideoPlaying)
            {
                GameObject.FindWithTag("VideoThumbnail").SendMessage("HideItself");
                GameObject.FindWithTag("HomeButton").SendMessage("HideItself");
                GameObject.Find("BackToVideosButton").SendMessage("ShowItself");
                HideSceneHeader();
                AudioButtons.SendMessage("ShowVideoButtons");
            }
            else if (!isVideoPlaying)
            {
                GameObject.FindWithTag("VideoThumbnail").SendMessage("ShowItself");
                GameObject.FindWithTag("HomeButton").SendMessage("ShowItself");
                GameObject.Find("BackToVideosButton").SendMessage("HideItself");
                ShowSceneHeader();
                AudioButtons.SendMessage("HideVideoButtons");
                StopVideo();
            }
        }
        else
        {
            GameObject.FindWithTag("HomeButton").SendMessage("ShowItself");
            GameObject.Find("BackToVideosButton").SendMessage("HideItself");
        }
    }



    public void SetVideoUrlInParent(string url)
    {
        clickedThumbnailVideoUrl = url;
    }

    void PlayVideo()
    {
        //VideoClip sourceVideo = getVideoByName(clickedThumbnailVideoName);
        //video.clip = sourceVideo;
        video.source = VideoSource.Url;
        video.url = ((Media)GlobalDataHolder.GetRouteParams("video")).data_url;
        video.frame = 0;
        video.Play();
        videoRenderer.enabled = true;
    }

    void StopVideo()
    {
        video.frame = 0;
        video.Stop();
        videoRenderer.enabled = false;
        isVideoPlaying = false;
    }

    void ControlVideo(string control)
    {
        if (control.Equals("play")) video.Play();
        else if (control.Equals("stop")) video.Pause();
        else if (control.Equals("restart"))
        {
            video.frame = 0;
            video.Play();
        }
    }

    VideoClip getVideoByName(string name)
    {
        foreach (VideoClip clip in videoClips)
            if (clip.name == name)
                return clip;
        return null;
    }

    void PlayingVideo(bool isPlaying)
    {
        isVideoPlaying = isPlaying;
    }


    void HideSceneHeader()
    {
        CanvasGroup SceneHeaderCanvasGroup = GameObject.Find("SceneHeader").GetComponent<CanvasGroup>();
        SceneHeaderCanvasGroup.alpha = 0;
        SceneHeaderCanvasGroup.interactable = false;
    }

    void ShowSceneHeader()
    {
        CanvasGroup SceneHeaderCanvasGroup = GameObject.Find("SceneHeader").GetComponent<CanvasGroup>();
        SceneHeaderCanvasGroup.alpha = 1;
        SceneHeaderCanvasGroup.interactable = true;
    }
}
