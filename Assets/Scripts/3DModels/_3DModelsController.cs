﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class _3DModelsController : MonoBehaviour
{
    SystemObj selectedSistemOBJ;


    // Start is called before the first frame update
    void Start()
    {
        
    }

    void Awake()
    {
        selectedSistemOBJ = (SystemObj)GlobalDataHolder.GetRouteParams("selectedSistemOBJ");
        foreach (GameObject Obj in GameObject.FindGameObjectsWithTag("3DModel"))
        {
            if (Obj.GetComponent<Attributes>().system_id == selectedSistemOBJ.id)
            {
                Obj.transform.SetParent(transform);
                Obj.GetComponent<RectTransform>().sizeDelta = new Vector2(160, 30);
            }
            else
            {
                hide(Obj);
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void hide(GameObject obj)
    {
        obj.GetComponent<CanvasGroup>().interactable = false;
        obj.GetComponent<CanvasGroup>().alpha = 0;
    }
}
