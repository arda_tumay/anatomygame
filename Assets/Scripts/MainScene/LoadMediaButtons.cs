﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LoadMediaButtons : MonoBehaviour
{
    public GameObject mediaButtonPrefab;
    GameObject btn;
    // Start is called before the first frame update
    void Start()
    {
        List<MediaButton> buttons = new List<MediaButton>();
        buttons.Add(new MediaButton("0", "Resimler", "Resimler"));
        buttons.Add(new MediaButton("1", "Videolar", "Videolar"));
        buttons.Add(new MediaButton("2", "DicomSliders", "Dicom Sliders"));
        buttons.Add(new MediaButton("3", "3DModels", "3D Models"));


        if (mediaButtonPrefab)
        {
            buttons.ForEach((item) =>
            {
                btn = Instantiate(mediaButtonPrefab, transform) as GameObject;
                btn.GetComponent<ButtonProperties>().SendMessage("setMediaButton", item);
                btn.GetComponentInChildren<Text>().text = item.buttonText;
                btn.GetComponent<RectTransform>().sizeDelta = new Vector2(100, 100);
                btn.GetComponent<Button>().onClick.AddListener(delegate { onClickButton(item); });
            });
        }
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void onClickButton(MediaButton btn)
    {
        SceneManager.LoadScene(btn.router);
    }

}

public class MediaButton
{
    public string id { get; set; }
    public string router { get; set; }
    public string buttonText { get; set; }
    
    public MediaButton(string id, string router, string buttonText)
    {
        this.id = id;
        this.router = router;
        this.buttonText = buttonText;
    }
}