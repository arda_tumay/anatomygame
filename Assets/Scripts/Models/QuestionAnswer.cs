﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

class QuestionAnswer
{
    public int id { get; set; }
    public Question question { get; set; }
    public Answer answer { get; set; }

}
