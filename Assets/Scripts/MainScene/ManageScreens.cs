﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.Video;

public class ManageScreens : MonoBehaviour
{

    //enum Sistemler { Yapilar, HareketSistemi, DolasimSistemi, SinirSistemi, UrogenitalSistem, SolunumSistemi, EndokrinSistem, SindirimSistemi, PatolojikOgeler };


    public Text textHints;
    public Button Oyunlar;
    public Button Medya;
    public Button Iletisim;
    public Button Hakkımızda;
    public Button Cikis;
    public Button Quiz;
    public Button Mainmenu;

    List<SystemObj> systems;

    int selectedOption;

    // Use this for initialization
    void Start()
    {
        systems = new List<SystemObj>();
        StartCoroutine(GetSystems());
        //GameObject.Find("SkullThumb").SetActive(true);
        //GameObject.Find("SkullThumb").GetComponent<CanvasGroup>().alpha = 0;
        //GameObject.Find("SkullThumb").GetComponent<CanvasGroup>().blocksRaycasts = false;

        //GameObject.Find("DicomSlider").SetActive(true);
        //GameObject.Find("DicomSlider").GetComponent<CanvasGroup>().alpha = 0;
        //GameObject.Find("DicomSlider").GetComponent<CanvasGroup>().blocksRaycasts = false;

        GameObject.Find("Mainmenu").GetComponent<CanvasGroup>().alpha = 0;
        GameObject.Find("Mainmenu").GetComponent<CanvasGroup>().interactable = false;
        GameObject.Find("MediaPanel").GetComponent<CanvasGroup>().alpha = 0;
        GameObject.Find("MediaPanel").GetComponent<CanvasGroup>().interactable = false;

        textHints.enabled = false;
        DisableButtons();
        //HideResimlerButtons();
        //HideHomeButton();

        if (gameObject.GetComponent<Dropdown>() != null)
        {
            gameObject.GetComponent<Dropdown>().onValueChanged.AddListener(DropdownValueChanged);
        }
        if (Oyunlar)
        {
            Oyunlar.onClick.AddListener(OyunlarClicked);
        }
        if (Medya)
        {
            Medya.onClick.AddListener(MedyaClicked);
        }
        if (Iletisim)
        {
            Iletisim.onClick.AddListener(IletisimClicked);
        }
        if (Hakkımızda)
        {
            Hakkımızda.onClick.AddListener(HakkımızdaClicked);
        }
        if (Quiz)
        {
            Quiz.onClick.AddListener(QUIZClicked);
        }
        if (Cikis)
        {
            Cikis.onClick.AddListener(CikisClicked);
        }
        if (Mainmenu)
        {
            Mainmenu.onClick.AddListener(MainmenuClicked);
        }
    }


    // Update is called once per frame
    void Update()
    {

    }

    IEnumerator GetSystems()
    {
        UnityWebRequest www = UnityWebRequest.Get("http://188.166.49.57:8080/Systems");
        yield return www.SendWebRequest();

        if (www.isNetworkError || www.isHttpError)
        {
            Debug.Log(www.error);
        }
        else
        {
            // Show results as text
            string systemsString = www.downloadHandler.text;
            //JArray a = JArray.Parse(systemsString);

            //  Debug.Log(a.ToString());
            List<SystemObj> result = JsonConvert.DeserializeObject<List<SystemObj>>(systemsString);
            result.Insert(0, new SystemObj(-1, "Yapılar"));
            List<Dropdown.OptionData> listOfSystems = new List<Dropdown.OptionData>();
            //listOfSystems.Add(new Dropdown.OptionData("Yapılar"));
            result.ForEach((response) =>
            {
                //Debug.Log(response.name);
                listOfSystems.Add(new Dropdown.OptionData(response.name.ToString()));
            });
            GameObject DropdownObject = GameObject.Find("SistemlerDropdown");
            Dropdown SystemsDropwdown = DropdownObject.GetComponent<Dropdown>();
            SystemsDropwdown.options = listOfSystems;
            systems = result;
            /* foreach (KeyValuePair<string, object> d in systems)
             {
                 // if (d.Value.GetType().FullName.Contains("Newtonsoft.Json.Linq.JObject"))
                 if (d.Value is JObject)
                 {
                     Debug.Log(d.Value);
                 }
                 else
                 {
                     Debug.Log(d.Value);
                 }
             }*/

            // Or retrieve results as binary data
            /* byte[] results = www.downloadHandler.data;
             foreach(int i in results)
             {
                 Debug.Log((char)results[i]);

             }*/
        }
    }

    void OyunlarClicked()
    {
        /*if (selectedOption == (int)GlobalDataHolder.Sistemler.SinirSistemi)
        {
            SceneManager.LoadScene("SinirSistemi");
        }*/
        SceneManager.LoadScene("Oyunlar");
    }
    void MedyaClicked()
    {
        HideMainMenu();
        GameObject.FindWithTag("TanıtımImage").GetComponent<Image>().enabled = false;
        GameObject.Find("MediaPanel").GetComponent<CanvasGroup>().alpha = 1;
        GameObject.Find("MediaPanel").GetComponent<CanvasGroup>().interactable = true;
        GameObject.Find("Mainmenu").GetComponent<CanvasGroup>().alpha = 1;
        GameObject.Find("Mainmenu").GetComponent<CanvasGroup>().interactable = true;
    }

    void IletisimClicked()
    {

    }
    void HakkımızdaClicked()
    {

    }
    void MainmenuClicked()
    {
        ShowMainMenu();
        GameObject.FindWithTag("TanıtımImage").GetComponent<Image>().enabled = true;
        GameObject.Find("MediaPanel").GetComponent<CanvasGroup>().alpha = 0;
        GameObject.Find("MediaPanel").GetComponent<CanvasGroup>().interactable = false;
        GameObject.Find("Mainmenu").GetComponent<CanvasGroup>().alpha = 0;
        GameObject.Find("Mainmenu").GetComponent<CanvasGroup>().interactable = false;
    }
    void QUIZClicked()
    {
        SceneManager.LoadScene("Quizler");
    }

    void CikisClicked()
    {
        Application.Quit();
    }
    void DropdownValueChanged(int selectedOption)
    {
        Debug.Log(selectedOption);
        this.selectedOption = selectedOption;
        GlobalDataHolder.Sistemler selectedSistem = GetSelectedSistem();
        GlobalDataHolder.PutRouteParams("selectedSistemOBJ", systems[selectedOption]);
        Debug.Log(systems[selectedOption].name);
        Debug.Log(systems[selectedOption].id);
        GlobalDataHolder.PutRouteParams("selectedSistem", selectedSistem);

        //textHints.enabled = true;
        if (selectedOption == (int)GlobalDataHolder.Sistemler.Yapilar)
        {
            DisableButtons();

            //GameObject.FindWithTag("TanıtımImage").SetActive(true);
            //setActive methodu objeyi tamamen disable ederek oyundan çıkarıyor. Obje daha sonra herhangi bi scriptten erişilemez oluyor ve erişilmek istenirse nullException hatası atıyor.
        }
        else
        {
            EnableButtons();
        }
    }

    private GlobalDataHolder.Sistemler GetSelectedSistem()
    {
        return (GlobalDataHolder.Sistemler)selectedOption;
    }

    public void ShowMainMenu()
    {
        GameObject buttonHolder = GameObject.FindWithTag("ButtonHolder");
        buttonHolder.GetComponent<CanvasGroup>().alpha = 1;
        buttonHolder.GetComponent<CanvasGroup>().blocksRaycasts = true;
    }

    public void HideMainMenu()
    {
        GameObject buttonHolder = GameObject.FindWithTag("ButtonHolder");
        buttonHolder.GetComponent<CanvasGroup>().alpha = 0; //this makes everything transparent
        buttonHolder.GetComponent<CanvasGroup>().blocksRaycasts = false; //this prevents the UI element to receive input events
    }

    /*public void ShowResimlerButtons()
    {
        GameObject buttonHolder = GameObject.FindWithTag("ResimlerSubmenu");
        buttonHolder.GetComponent<CanvasGroup>().alpha = 1;
        buttonHolder.GetComponent<CanvasGroup>().blocksRaycasts = true;
    }

    public void HideResimlerButtons()
    {
        GameObject buttonHolder = GameObject.FindWithTag("ResimlerSubmenu");
        buttonHolder.GetComponent<CanvasGroup>().alpha = 0; //this makes everything transparent
        buttonHolder.GetComponent<CanvasGroup>().blocksRaycasts = false; //this prevents the UI element to receive input events
    }*/

    /*public void ShowHomeButton()
    {
        GameObject buttonHolder = GameObject.FindWithTag("HomeButton");
        buttonHolder.GetComponent<CanvasGroup>().alpha = 1;
        buttonHolder.GetComponent<CanvasGroup>().blocksRaycasts = true;
    }*/

    /* public void HideHomeButton()
     {
         GameObject buttonHolder = GameObject.FindWithTag("HomeButton");
         buttonHolder.GetComponent<CanvasGroup>().alpha = 0; //this makes everything transparent
         buttonHolder.GetComponent<CanvasGroup>().blocksRaycasts = false; //this prevents the UI element to receive input events
     }*/

    void EnableButtons()
    {
        Oyunlar.interactable = true;
        Medya.interactable = true;
        Quiz.interactable = true;
    }

    void DisableButtons()
    {
        Oyunlar.interactable = false;
        Medya.interactable = false;
        Quiz.interactable = false;
    }
}
