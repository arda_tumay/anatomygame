﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class ButtonsController : MonoBehaviour {
    public float speed;
    public Button button;
    GameObject holder;
    Boolean gameClicked = false;
    Vector3 destination;
	// Use this for initialization
	void Start () {
        holder = GameObject.Find("ButtonHolder");
        Debug.Log(holder);
        button.onClick.AddListener(gamesButtonClicked);
        destination = new Vector3(holder.transform.position.x, holder.transform.position.y, holder.transform.position.z);
    }

    private void gamesButtonClicked()
    {
        if(gameClicked == false)
        {
            destination = new Vector3(holder.transform.position.x - 70, holder.transform.position.y + 40, holder.transform.position.z);

        }
        gameClicked = true;
       /* Vector3 v = new Vector3(holder.transform.position.x- 170, holder.transform.position.y+60, holder.transform.position.z);
        holder.transform.SetPositionAndRotation(v, holder.transform.rotation);*/
    }

    // Update is called once per frame
    void Update () {
        if (gameClicked)
        {
            if (destination != holder.transform.position)
            {
                // Move towards the destination each frame until the object reaches it
                IncrementPosition();
            }
        }
	}
    void IncrementPosition()
    {
        // Calculate the next position
        float delta = speed * Time.deltaTime;
        Vector3 currentPosition = holder.transform.position;
        Vector3 nextPosition = Vector3.MoveTowards(currentPosition, destination, delta);

        // Move the object to the next position
        holder.transform.position = nextPosition;
    }
}
