﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class HomeButton : MonoBehaviour
{

    // Use this for initialization
    void Start()
    {
        gameObject.GetComponent<Button>().onClick.AddListener(OnClick);
        Debug.Log("başladı");
    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnClick()
    {
        // this object was clicked - do something   
        //GlobalDataHolder.RemoveRouteParams("selectedSistem");
        GlobalDataHolder.ClearGlobalDataHolder();
        SceneManager.LoadScene("main");
    }

    public void ShowItself()
    {
        GameObject buttonHolder = GameObject.FindWithTag("HomeButton");
        buttonHolder.GetComponent<CanvasGroup>().alpha = 1;
        buttonHolder.GetComponent<CanvasGroup>().blocksRaycasts = true;
    }

    public void HideItself()
    {
        GameObject buttonHolder = GameObject.FindWithTag("HomeButton");
        buttonHolder.GetComponent<CanvasGroup>().alpha = 0; //this makes everything transparent
        buttonHolder.GetComponent<CanvasGroup>().blocksRaycasts = false; //this prevents the UI element to receive input events
    }

    public void SetText(string text)
    {
        Text tx = GetComponentInChildren<Text>();
        tx.text = text;
    }
}
