﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StaticResource
{

    public int id { get; set; }
    public string resource_name { get; set; }
    public string target_page { get; set; }
    public string button_image { get; set; }
    public string button_image_url { get; set; }
    public SystemObj system { get; set; } 
    public int system_id { get; set; }
    public StaticResourceTypes resource_type { get; set; }

}
